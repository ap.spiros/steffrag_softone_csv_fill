<?php

//load color-map csv
$file_color_mapping = fopen('steffrag.softone-fields.color-mapping.csv', 'r');
$cm_header = fgetcsv($file_color_mapping);
$arr_color_mapping = [];
$arr_color_mapping[] = $cm_header;
while ($row = fgetcsv($file_color_mapping)) {
  $arr_color_mapping[] = $row;
}
fclose($file_color_mapping);

//load core csv
$file = fopen('steffrag.softone-fields.csv', 'r');
$header = fgetcsv($file);

//our new dataset
$dataset = [];
$dataset[] = $header;

$style_failures = $season_failures = $size_failures = $master_sku_failures = [];
while ($row = fgetcsv($file)) {

  FieldHelper::generateStyle($row[2], $row[0], $row[4], $style_failures);                            //sku, name, style
  FieldHelper::generateSeason($row[2], $row[5], $season_failures);                                   //sku, season
  FieldHelper::generateBikiniPart($row[2], $row[16], $row[17]);                                      //sku, Top or Bottom, Bikini Part
  FieldHelper::generateMasterSku($row[2], $row[16], $row[1], $master_sku_failures);                  //sku, Top or Bottom, masterSku
  FieldHelper::generateSize($row[2], $row[24], $size_failures);                                      //sku, size
  FieldHelper::generateCategs($row[2], $row[15], $row[16], $row[18], $row[19], $row[20]);            //sku, Category, Top or Bottom, categ1, categ2, categ3
  FieldHelper::generateColorMapAndPattern($row[2], $row[21], $row[22], $row[23], $arr_color_mapping);   //sku, Color, color-map, pattern

  $dataset[] = $row;
}

// Print errors
FieldHelper::printErrors($style_failures, "generateStyle");
FieldHelper::printErrors($season_failures, "generateSeason");
FieldHelper::printErrors($size_failures, "generateSize");
FieldHelper::printErrors($master_sku_failures, "generateMasterSku");


// Create new csv
if (count($dataset) > 1) {
  $fp = fopen('steffrag.softone-fields.final.csv', 'w');
  foreach ($dataset as $row) {
    fputcsv($fp, $row);
  }
  fclose($file);
  fclose($fp);
}


class FieldHelper
{

  private static $style_reg = "/(^[A-Za-z]+[-|\w]){1}/";
  private static $season_reg = "/\d{2}+/";
  private static $master_sku_reg = "/-{1}/";
  private static $size_reg = "/-{1}/";

  public static function generateStyle($sku, $name, &$style, &$style_failures)
  {
    $match = [];
    if (preg_match(self::$style_reg, $name, $match)) {
      if (strtoupper($match[0]) == "THE") { //Multiword Product :(
        $style_failures[] = $sku;
      } else {
        $match[0] = str_replace("-", "", $match[0]);
        $style = strtoupper($match[0]);
      }
    } else {
      $style_failures[] = $sku;
    }
  }

  public static function generateSeason($sku, &$season, &$season_failures)
  {
    $match = [];
    if (preg_match(self::$season_reg, $sku, $match)) {
      $season = "20" . $match[0];
    } else {
      $season_failures[] = $sku;
    }
  }

  public static function generateBikiniPart($sku, $TorB, &$bikiniPart)
  {
    if (trim($TorB) == 'TOP' || trim($TorB) == 'BOTTOM') {
      $bikiniPart = trim($TorB);
    }
  }

  public static function generateSize($sku, &$size, &$size_failures)
  {
    $match = [];
    if (preg_match(self::$size_reg, $sku, $match, PREG_OFFSET_CAPTURE, 0)) {
      $needle = $match[0][1];
      $new_size = trim(substr($sku, $needle + 1));
      if ($new_size == "O/S" || $new_size == "ONE SIZE" || $new_size == "OS") {
        $size = $new_size = "O/S";
      } elseif ($new_size == "xs"){
        $size = "XS";
      } else {
        $size = $new_size;
      }
      if (strlen($new_size) > 3) {
        $size = "";
        $size_failures[] = $sku;
      }
    } else {
      $size_failures[] = $sku;
    }
  }

  public static function generateMasterSku($sku, $TorB, &$masterSku, &$master_sku_failures)
  {
    $match = [];
    if (preg_match(self::$master_sku_reg, $sku, $match, PREG_OFFSET_CAPTURE, 0)) {
      $needle = $match[0][1];
      if (trim($TorB) == 'TOP' || trim($TorB) == 'BOTTOM') {
        $masterSku = trim(substr($sku, 0, $needle - 1));
      } else {
        $masterSku = trim(substr($sku, 0, $needle));
      }
    } else {
      //didnt find "-", so put it as it is
      $masterSku = $sku;
      //not actually a failure
      $master_sku_failures[] = $sku;
    }
  }

  public static function generateCategs($sku, $category, $TorB, &$categ1, &$categ2, &$categ3)
  {
    //Classics Term
    if (preg_match('/\b(Classics)+\b/i', $category, $match, PREG_OFFSET_CAPTURE, 0)) {
      $categ3 = 'Classics';
    }

    //Athleisure Term
    if ($TorB == 'FITNESS') {
      $categ1 = 'Athleisure';
      return;
    } else {
      $match = [];
      if (preg_match('/\b(Athleisure)+\b/i', $category, $match, PREG_OFFSET_CAPTURE, 0)) {
        $categ1 = 'Athleisure';
        return;
      }
    }

    //Beachware Term
    if ($TorB == 'BEACHWEAR') {
      $categ1 = 'Beachwear';
      return;
    } else {
      $match = [];
      if (preg_match('/\b(BEACHWEAR)+\b/i', $category, $match, PREG_OFFSET_CAPTURE, 0)) {
        $categ1 = 'Beachwear';
        return;
      }
    }

    //Kids > Bikini || One Piece   ( only 3 entries )
    $match = [];
    if (preg_match('/\b(Kids)+\b/i', $category, $match, PREG_OFFSET_CAPTURE, 0)) {
      $categ1 = 'Kids';
    }

    //Swimwear > Bikini || One Piece
    if (trim($TorB) == 'TOP' || trim($TorB) == 'BOTTOM') {
      $categ2 = 'Bikini';
      if ($categ1 == '') {
        $categ1 = 'Swimwear';
      }
    } elseif (trim($TorB) == 'ONE PIECE' || trim($TorB) == 'ONEPIECE') {
      $categ2 = 'One Piece';
      if ($categ1 == '') {
        $categ1 = 'Swimwear';
      }
    }
  }

  public static function generateColorMapAndPattern($sku, $color, &$color_map, &$pattern, $arr_color_mapping)
  {

    foreach ($arr_color_mapping as $each_color_mapping) {

      if (trim($each_color_mapping[0]) == trim($color)) {
        $color_map = $each_color_mapping[1];
        $pattern = $each_color_mapping[2];

        break;
      }
    }
  }

  public static function printErrors($failures, $for)
  {
    if (count($failures) > 0) {
      if ($for == "generateMasterSku") {
        print_r("\nSKUS without '-' (mastersku=sku): \n");
      } else {
        print_r("\nFailed to " . $for . " for SKUs: \n");
      }
      foreach ($failures as $failure) {
        print_r($failure . "\n");
      }
    } else {
      print_r("\n No Failures for " . $for . "\n");
    }
  }
}
